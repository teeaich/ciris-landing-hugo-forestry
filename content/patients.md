+++
date = "2018-06-11T10:10:25Z"
title = "CIRIS - Die Plattform für Patienten"
type = "page"
[menu.main]
identifier = "patients"
name = "FÜR PATIENTEN"
parent = "DIE PLATFORM"
weight = 1
[[blocks]]
alignment_of_text = "center"
background = ""
color = "#0390CF"
coloured_string = "Patienten"
headline = "CIRIS - Die Plattform für Patienten"
template = "text-content"
text = ""
[[blocks]]
background = "white"
template = "slider"
[[blocks.slider]]
heading = "PATIENTEN-TRANSPORT"
slider_image = "/uploads/2018/06/09/slider1.jpg"
subheading = "Mehr Patientenkomfort durch kürzere Wartezeiten"
[[blocks]]
alignment_of_text = "left"
background = ""
color = ""
coloured_string = ""
headline = ""
template = "text-content"
text = "Der Bedarf einer medizinischen Behandlung setzt einen Patienten bereits unter Stress. Man möchte ausschließlich an die anschließende Genesung denken und sich nicht mit logistischen oder organisatorischen Problemen auseinandersetzen. Sie möchten sich weder mit langen Wartezeiten beim Arzt noch bei den Krankentransporten herumschlagen? CIRIS nimmt sich diesen Problemen an und ermöglicht Ihnen einen reibungslosen Ablauf, damit Sie schnellstmöglich zurück in vertrauter Umgebung sind. Das Wohlbefinden von Ihnen ist unsere Mission."
[[blocks]]
divider = false
heading = ""
template = "icon-box-content"
[[blocks.icon_box_element]]
icon = "fa-wheelchair"
text = "Ihre Fahrt zum Arzt oder ins Krankenhaus ist jedes Mal aufs Neue eine Herausforderung. Sie benötigen das richtige Transportmittel, denn nicht jedes Fahrzeug kann Sie angemessen an Ihr Ziel bringen. Sie sind die aufwändige Planung leid, bei der Sie sich noch Gedanken darum machen, ob der Ein- und Ausstieg bei dem jeweiligen Fahrzeug problemlos klappt? Dies gehört ab jetzt der Vergangenheit an. CIRIS übernimmt die komplette Planung Ihrer Fahrt, wählt das passende Fahrzeug, minimiert die Wartezeit und befreit Sie von Ihren Sorgen damit Sie sich nur um sich kümmern können."
title = "Rollstuhltransport"
title_color = ""
[[blocks.icon_box_element]]
icon = "fa-thumbs-o-up"
text = "Sie haben wiederkehrende Behandlungen, welche Sie bereits in Ihren Alltag integriert haben. Mehrmals die Woche fahren Sie zur Behandlung. In der Regel sind die Abläufe in der medizinischen Einrichtung entsprechend routiniert und laufen reibungslos ab, der Transport ist die einzige Unbekannte in diesem Ablauf. Sie wünschen sich rechtzeitig zu Ihrem Termin zu erscheinen und direkt danach wieder die Rückfahrt antreten zu können? Diesen Wunsch erfüllen wir Ihnen. CIRIS plant Ihre wiederkehrenden Fahrten und meldet dem Transportunternehmen Ihre exakten Ankunfts- und Abfahrtszeiten."
title = "Serientransport"
title_color = ""
[[blocks.icon_box_element]]
icon = "fa-desktop"
text = "Sie sind derzeit in einer Ausnahmesituation. Ihre momentanen Behandlungen haben für Sie höchste Priorität. Für Sie zählt nur: Gesund werden! Die komplette Bürokratie und die organisatorischen Hürden verstehen Sie nicht so ganz? Und der Transport zu den medizinischen Einrichtungen interessiert Sie am Wenigsten? Gut so! Denn dies erledigen wir. CIRIS koordiniert Ihre akuten Fahrten in die jeweiligen Fachkliniken reibungslos und ohne Rückfragen. Sie müssen sich weder um die Organisation noch um die Abrechnung der Transporte kümmern, sondern können sich rein auf Ihre Genesung konzentrieren."
title = "Ausnahmefalltransport"
title_color = ""
[[blocks]]
alignment_of_text = "center"
background = ""
color = "#0390CF"
coloured_string = "Patienten"
headline = "CIRIS bietet den Patienten"
template = "text-content"
text = ""
[[blocks]]
divider = false
heading = ""
template = "icon-box-content"
[[blocks.icon_box_element]]
icon = "fa-plus-square"
text = "Kommen Sie schneller zu Ihrer Behandlung und wieder zurück!"
title = "SCHNELLIGKEIT"
title_color = "#0390CF"
[[blocks.icon_box_element]]
icon = "fa-plus-square"
text = "Sie können den Transport ganz einfach über Ihr Smartphone bestellen."
title = "EINFACHE BEDIENUNG"
title_color = "#0390CF"
[[blocks.icon_box_element]]
icon = "fa-plus-square"
text = "CIRIS lässt sich ganz einfach in Ihren derzeitigen Alltag integrieren."
title = "ALLTAGSTAUGLICH"
title_color = "#0390CF"

+++
