+++
date = "2018-06-11T10:10:25Z"
title = "CIRIS - Die Vermittung für medizinische Transportunternehmen"
type = "page"
[menu.main]
identifier = "transportcenter"
name = "FÜR TRANSPORTUNTERNEHMEN"
parent = "DIE PLATFORM"
weight = 3
[[blocks]]
alignment_of_text = "center"
background = ""
color = "#0057a0"
coloured_string = "medizinische Transportunternehmen"
headline = "CIRIS - Die Vermittung für medizinische Transportunternehmen"
template = "text-content"
text = ""
[[blocks]]
background = "white"
template = "slider"
[[blocks.slider]]
heading = "KRANKEN-TRANSPORT"
slider_image = "/uploads/2018/06/11/slider2.jpg"
subheading = "Mehr Fahrten und höhere Planbarkeit"
[[blocks]]
alignment_of_text = "left"
background = ""
color = ""
coloured_string = ""
headline = ""
template = "text-content"
text = "Jede medizinische Behandlung beginnt mit der Fahrt zur jeweiligen medizinischen Einrichtung. Hierfür sind Sie verantwortlich und sie führen diese Fahrten mit voller Hingabe durch. Ihre Patienten kennen Sie häufig schon sehr gut und wissen, dass sie gut aufgehoben sind und wohlbehalten an Ihr Ziel kommen. Doch hinter den Kulissen stehen auch die Transportzahlen und der Druck, mehr Fahrten mit Ihren begrenzten Kapazitäten leisten zu können. Die fehlende Planbarkeit dieser Einsätze macht dies zu einer komplizierten Gratwanderung und führt zu erhöhtem Stress. CIRIS unterstützt Sie bei der Organisation, Delegierung und Durchführung Ihrer Fahrten und ermöglicht es Ihnen, dadurch mehr Patienten befördern zu können. Damit Sie sich komplett um das Wohl der Patienten kümmern können."
[[blocks]]
divider = false
heading = ""
template = "icon-box-content"
[[blocks.icon_box_element]]
icon = "fa-user"
text = "Derzeit können sie ausschließlich wiederkehrende Fahrten mit Ihren Fahrzeugen vorausschauend einplanen. Dadurch entstehen unter Leerfahrten und längere Wartezeiten, welche selten durch spontane Fahrten gefüllt werden können. CIRIS ermöglicht Ihnen durch einfache Eingaben , mehr Fahrten durch strukturierte Routenplanung durchführen zu können."
title = "Patientenanzahl"
title_color = ""
[[blocks.icon_box_element]]
icon = "fa-clock-o"
text = "Bei fehlender Planung und gleichbleibenden Kapazitäten ist es nahezu unmöglich, die Anzahl der Fahrten in der gleichen Zeit zu erhöhen. CIRIS kann dank frühzeitiger Planung Ihrer Tagesrouten einen effizienteren Ablauf koordinieren. Zusätzlich eingehende Fahrten werden automatisch in die Routenplanung eingefügt, sobald ein Zeitfenster verfügbar ist."
title = "Planbarkeit"
title_color = ""
[[blocks.icon_box_element]]
icon = "fa-search-plus"
text = "Ihre Fahrzeuge sind kontinuierlich im Einsatz und bringen Patienten zu Ihrem Ziel. Mit CIRIS sehen Sie On-Demand den Standort und Status Ihrer Fahrzeuge und können deren Routen optimieren um weitere Ressourcen einzusparen oder Aufträge zu erledigen. Über ein durchgehendes Monitoring Ihrer Flotte können sie die Kapazitäten Ihres Unternehmens voll ausschöpfen."
title = "Transparenz"
title_color = ""
[[blocks]]
alignment_of_text = "center"
background = ""
color = "#0057a0"
coloured_string = "/develop/ciris-landing-hugo-forestry/content/transportcenter.md"
headline = "CIRIS bietet den medizinischen Transportunternehmen"
template = "text-content"
text = ""
[[blocks]]
divider = false
heading = ""
template = "icon-box-content"
[[blocks.icon_box_element]]
icon = "fa-plus-square"
text = "Sie können Ihre vorhandenen Kapazitäten ideal einsetzen."
title = "MEHR FAHRTEN"
title_color = "#0057a0"
[[blocks.icon_box_element]]
icon = "fa-plus-square"
text = "Sie erhalten optimierte Routen und automatisierte Planungsmöglichkeiten."
title = "OPTIMIERUNG"
title_color = "#0057a0"
[[blocks.icon_box_element]]
icon = "fa-plus-square"
text = "Sie sehen kontinuierlich wo sich Ihre Fahrzeuge in welchem Status befinden."
title = "TRANSPARENZ"
title_color = "#0057a0"

+++
