+++
date = "2018-06-11T10:10:25Z"
title = "CIRIS - Patiententransporte für Ihre medizinische Einrichtung"
type = "page"
[menu.main]
identifier = "medicalcenter"
name = "FÜR MEDIZINISCHE EINRICHTUNGEN"
parent = "DIE PLATFORM"
weight = 2
[[blocks]]
alignment_of_text = "center"
background = ""
color = "#0390CF"
coloured_string = "medizinische Einrichtung"
headline = "CIRIS - Patiententransporte für Ihre medizinische Einrichtung"
template = "text-content"
text = ""
[[blocks]]
background = "white"
template = "slider"
[[blocks.slider]]
heading = "PATIENTEN-TRANSPORT"
slider_image = "/uploads/2018/06/11/slider3.jpg"
subheading = "Schnellere Prozesse und glücklichere Patienten"
[[blocks]]
alignment_of_text = "left"
background = ""
color = ""
coloured_string = ""
headline = ""
template = "text-content"
text = "Sobald ein Patient bei Ihnen ankommt, kümmern Sie sich mit voller Hingabe um dessen Genesung und Zufriedenheit. Das ist Ihr Beruf und Ihre Passion. CIRIS teilt diese Passion und hilft Ihnen dabei, gemeinsam noch mehr für das Wohl des Patienten zu tun. Wir wollen den Patienten nicht lange im Wartezimmer warten lassen. CIRIS bietet Ihnen gut organisierte und zeitnahe Transporte. Wir wollen ein angemessenes Transportmittel für jeden individuellen Patienten. CIRIS klassifiziert die Fahrten, wählt das Transportmittel und unterstützt hierbei noch unser Gesundheitssystem durch signifikante Kosteneinsparungen."
[[blocks]]
divider = false
heading = ""
template = "icon-box-content"
[[blocks.icon_box_element]]
icon = "fa-ambulance"
text = "Die Prozesse in Ihrer medizinischen Einrichtung sind routiniert und komplett aufeinander abgestimmt. Dadurch werden Ihre Patienten schnellst möglichst und in bester Qualität behandelt. Doch was vor und nach der Behandlung passiert, liegt nicht in Ihrer Hand. CIRIS bringt Licht ins Dunkle und macht den Transport von Patienten für Sie sichtbar. Wir bieten Ihnen Transparenz und Planbarkeit."
title = "Wann kommt mein Patient?"
title_color = ""
[[blocks.icon_box_element]]
icon = "fa-clock-o"
text = "Die Behandlung Ihres Patienten ist abgeschlossen und dieser wünscht sich nun, sicher nach Hause gebracht zu werden. Der Transportschein ist erstellt und Ihr Patient befindet sich erneut im Wartezimmer. Ab diesem Zeitpunkt heißt es warten, für den Patienten und für Sie. CIRIS lässt Ihre Patienten durch intelligente Routenplanung nicht lange warten. Zudem sehen Sie immer wann der bestellte Transport eintrifft."
title = "Warum wartet mein Patient noch?"
title_color = ""
[[blocks.icon_box_element]]
icon = "fa-file-text-o"
text = "Für jeden Patienten muss ein Transportschein ausgefüllt werden, abhängig vom Zustand, der Behandlung und der Mobilität des Patienten. Bei medizinischen Transporten können verschiedene Transportmittel gewählt werden. CIRIS übernimmt dies für Sie und klassifiziert den jeweiligen Transport individuell. Sie müssen lediglich online einen Transport bestellen und CIRIS bringt Ihren Patienten sicher nach Hause."
title = "Welchen Transportschein nehme ich?"
title_color = ""
[[blocks]]
alignment_of_text = "center"
background = ""
color = "#0390CF"
coloured_string = "medizinischen Einrichtungen"
headline = "CIRIS bietet den medizinischen Einrichtungen"
template = "text-content"
text = ""
[[blocks]]
divider = false
heading = ""
template = "icon-box-content"
[[blocks.icon_box_element]]
icon = "fa-plus-square"
text = "Sie wissen wann Ihre Patienten eintreffen und können die internen Prozesse darauf abstimmen."
title = "TRANSPARENZ"
title_color = "#0390CF"
[[blocks.icon_box_element]]
icon = "fa-plus-square"
text = "Bringen Sie Ihre Patienten schneller zur Behandlung und wieder nach Hause."
title = "SCHNELLIGKEIT"
title_color = "#0390CF"
[[blocks.icon_box_element]]
icon = "fa-plus-square"
text = "Treffen Sie schnell und einfach die richtige Auswahl des Transportmittels."
title = "SICHERHEIT"
title_color = "#0390CF"

+++
