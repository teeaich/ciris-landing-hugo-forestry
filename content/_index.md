+++
title = "CIRIS - Die Transportvermittlung für mehr Patientenkomfort"
type = "page"
[menu.main]
identifier = "home"
name = "STARTSEITE"
parent = ""
weight = 1
[[blocks]]
template = "slider"
[[blocks.slider]]
heading = "PATIENTEN"
slider_image = "/uploads/2018/06/09/slider1.jpg"
subheading = "Mehr Patientenkomfort durch kürzere Wartezeiten"
[[blocks.slider]]
heading = "EINFACH UND DIGITAL"
slider_image = "/uploads/2018/06/11/slider2.jpg"
subheading = "Unkomplizierte Online-Bestellung ohne Rückfragen"
[[blocks.slider]]
heading = "KUNDENORIENTIERT"
slider_image = "/uploads/2018/06/11/slider3.jpg"
subheading = "Schnelle Prozesse für medizinische Einrichtungen"
[[blocks.slider]]
heading = "EINE PLATTFORM"
slider_image = "/uploads/2018/06/11/slider4.jpg"
subheading = "Für Patienten, medizinsche Einrichtungen und Transportunternehmen"
[[blocks]]
alignment_of_text = "center"
background = "grey"
headline = "CIRIS - Die Transportvermittlung für mehr Patientenkomfort"
template = "text-content"
[[blocks]]
divider = true
template = "icon-box-content"
[[blocks.icon_box_element]]
icon = "fa-clock-o"
text = "Schnelle Disposition und kürzere Wartezeiten für Patienten"
title = "SCHNELL"
[[blocks.icon_box_element]]
icon = "fa-thumbs-o-up"
text = "Einfache Bestellung ohne Rückfragen"
title = "EINFACH"
[[blocks.icon_box_element]]
icon = "fa-desktop"
text = "Online Bestellung und ohne Rückfragen"
title = "DIGITAL"
[[blocks.icon_box_element]]
icon = "fa-user-plus"
text = "Für zufriedene Kunden und Patienten"
title = "KUNDENORIENTIERIT"
[[blocks]]
headline = "Aktueller Entwicklungsstand"
template = "image-box-content"
[[blocks.image_box_element]]
image = "/uploads/2018/06/11/imageBox1.png"
text = "Bei CIRIS ist es derzeit möglich, neue Transportanfragen in das System einzugeben."
title = "Eingabe von Transportfahrten"
[[blocks.image_box_element]]
image = "/uploads/2018/06/11/imageBox2.png"
text = "Dem User wird bei CIRIS die Möglichkeit gegeben, die Transportanfragen zu verwalten. Sowohl von der Eingabeseite, als medizinisches Zentrums, und auch von der Gegenstelle. Die Zeit bis zur Ankunft wird dem Kunden mitgeteilt."
title = "Verwalten von Transportanfragen"
[[blocks.image_box_element]]
image = "/uploads/2018/06/11/imageBox3.png"
text = "CIRIS erleichtert die Verwaltung von Transportanfragen für die Transportunternehmen. Gewünschte Patientenfahrten können Fahrern oder Fahrzeugen zugeordnet werden. In der Verwaltung der Fahrten wird auch die notwendige Startzeit des Fahrer, inklusive der Berücksichtigung des Verkehrs, angezeigt."
title = "Zuordnung der Transportanfragen"
[[blocks.image_box_element]]
image = "/uploads/2018/06/11/imageBox4.png"
text = "Dem Fahrer eines Patiententransportfahrzeuges wird über eine App der nächste Fahrauftrag zugesendet. Gleichzeizig bekommt er die notwendige Startzeit mitgeteilt. Den Status der Fahrt ändert er über die App. Gewünschte Patientenfahrten können Fahrern oder Fahrzeugen zugeordnet werden. In der Verwaltung der Fahrten wird auch die notwendige Startzeit des Fahrer, inklusive der Berücksichtigung des Verkehrs, angezeigt."
title = "Fahrzeug App"

+++
