+++
date = "2018-06-13T06:26:33Z"
title = "CIRIS - Kontakt"
type = "page"
[menu.main]
name = "KONTAKT"
weight = 4
[[blocks]]
template = "slider"
[[blocks.slider]]
heading = "PATIENTEN"
slider_image = "/uploads/2018/06/09/slider1.jpg"
subheading = "Mehr Patientenkomfort durch kürzere Wartezeiten"
[[blocks.slider]]
heading = "EINFACH UND DIGITAL"
slider_image = "/uploads/2018/06/11/slider2.jpg"
subheading = "Unkomplizierte Online-Bestellung ohne Rückfragen"
[[blocks.slider]]
heading = "KUNDENORIENTIERT"
slider_image = "/uploads/2018/06/11/slider3.jpg"
subheading = "Schnelle Prozesse für medizinische Einrichtungen"
[[blocks.slider]]
heading = "EINE PLATTFORM"
slider_image = "/uploads/2018/06/11/slider4.jpg"
subheading = "Für Patienten, medizinsche Einrichtungen und Transportunternehmen"
[[blocks]]
success = "Ihre Nachricht wurde gesendet.\n\nVielen Dank!"
template = "contact-form"

+++
