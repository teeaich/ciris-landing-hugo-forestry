+++
date = "2018-06-12T09:18:10Z"
title = "CIRIS - Das Unternehmen"
type = "page"
[menu.main]
identifiert = "company"
name = "DAS UNTERNEHMEN"
parent = "ÜBER UNS"
weight = 1
[[blocks]]
alignment_of_text = "center"
background = ""
color = ""
coloured_string = ""
headline = "CIRIS - Das Unternehmen"
template = "text-content"
text = ""
[[blocks]]
background = ""
template = "slider"
[[blocks.slider]]
heading = "EINE PLATTFORM"
slider_image = "/uploads/2018/06/11/slider4.jpg"
subheading = "Für Patienten, medizinsche Einrichtungen und Transportunternehmen"
[[blocks]]
alignment_of_text = "left"
background = ""
color = ""
coloured_string = ""
headline = ""
template = "text-content"
text = "### Vision\n\nDas langfristige Ziel von CIRIS ist es die Organisation von Patiententransporten zu revolutionieren. Hierbei plant die Unternehmung CIRIS mittels einer B2C Plattform das derzeit analoge System der Delegierung von Patiententransporten ins Zeitalter der Digitalisierung zu begleiten – mit dem Ziel alle Beteiligten profitieren zu lassen:\n\n* Schneller Transport für Patienten\n* Einfache Bestellung durch die medizinische Einrichtung\n* Mehr Transporte für das Patiententransportunternehmen\n\nDurch den Ansatz diese drei Parteien auf einer Plattform zusammenzubringen, schafft CIRIS eine Möglichkeit, die Kosten innerhalb des Gesundheitssystems maßgeblich zu senken.\n\n### **Mission**\n\nCIRIS kommt in erster Linie den Patienten zugute, indem benötigte Transporte weniger zeitintensiv gestaltet werden. Neben der Entlastung der Leitstelle sind die Rettungsdienst-Unternehmen und die Krankenkassen große Interessengruppen dieser Plattform. Durch die Wahl des richtigen Transportmittels ermöglicht CIRIS der Krankenkasse durch individuelle Klassifizierung der Fahrten eine Optimierung und daraus resultierende Einsparungen im Abrechnungssystem. Die verschiedenen Anbieter von Patienten-Transporten können Ihre Transportmittel durch intelligente Routenplanung besser auslasten und die Anzahl der Leerfahrten signifikant reduzieren.\n\n### Werte\n\nDer Name CIRIS setzt sich aus dem Wort „Care“ und der griechischen Götterbotin „Iris“ zusammen. Es entstand eine symbolische Parallele zwischen der Überbringung von Nachrichten aus der Mythologie und der Planung und Delegierung von Patiententransporten in unserer Zeit. CIRIS steht für Vertrauen, Zuverlässigkeit und Transparenz. Für unsere Kunden, unsere User und vor allem die Patienten."
[[blocks]]
alignment_of_text = "center"
background = ""
color = ""
coloured_string = ""
headline = "Das Team CIRIS"
template = "text-content"
text = "“To run an efficient team, you only need three people: a Hipster, a Hacker, and a Hustler.” Nachdem das Gründerteam bereits zwei dieser personifizierten Rollen abdeckt, wurde im späteren Verlauf noch ein Experte für Softwareentwicklung rekrutiert. Die ideale Team-Zusammenstellung für ein Tech-Startup war perfekt."
[[blocks]]
headline = ""
image_box_element = []
template = "image-box-content"
text = ""
[[blocks.image_box_rich_element]]
image = "/uploads/2018/06/12/Alexander.png"
text = "Alexander hat seinen Master of Science im Bereich der Medizintechnik absolviert. Während des Studiums konnte er über mehrere Projekte (u.a. bei der NASA) bereits Erfahrungen in verschiedenen Anwendungsgebieten der Medizintechnik sammeln. Zusätzlich kam die Tätigkeit als Rettungssanitäter CIRIS zugute. Seine persönliche Motivation war seitdem, das Gesundheitswesen mit seiner Arbeitskraft zu unterstützen. Daher startete er seine Karriere als Produktmanager in der Medizintechnik um weiter interdisziplinär tätig zu sein. Nun möchte er mit CIRIS dieses Ziel weiterverfolgen.\n\n##### Stärken\n\n* Rettungsdienst\n* Finanzplanung\n* Gesundheitswesen\n* Produktmanagement"
title = "Alexander Kunze"
[[blocks.image_box_rich_element]]
image = "/uploads/2018/06/12/Christian.png"
text = "Christian hat sein Ingenieursstudium in Medizin- und Automatisierungstechnik ebenfalls mit einem M.Sc. absolviert. Während und nach seinem Studium arbeitete er bereits im Bereich der Softwareentwicklung in renommierten Medizintechnikfirmen. Um den engen Austausch mit Kunden noch zu intensivieren, setzte er seine Karriere im Bereich des Produktmanagements fort. Der Entrepreneurship-Gedanke war bei ihm schon früh vorhanden und der Wunsch wuchs, etwas Eigenes aufzubauen. Dies war seine Motivation, neben seiner momentanen Tätigkeit am Startup CIRIS zu arbeiten.\n\n##### Stärken\n\n* Anforderungsanalyse\n* Business-Analyse\n* Projektmanagement\n* Produktmanagement"
title = "Christian Ruff"


+++
