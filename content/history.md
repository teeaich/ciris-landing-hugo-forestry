+++
date = "2018-06-12T10:45:12Z"
title = "CIRIS - Die Erfolgsgeschichte"
type = "page"
[menu.main]
identifier = "history"
name = "DIE GESCHICHTE"
parent = "ÜBER UNS"
weight = 2
[[blocks]]
alignment_of_text = "center"
background = ""
color = ""
coloured_string = ""
headline = "CIRIS - Die Erfolgsgeschichte"
template = "text-content"
text = ""
[[blocks]]
background = ""
template = "slider"
[[blocks.slider]]
heading = "PATIENTEN"
slider_image = "/uploads/2018/06/09/slider1.jpg"
subheading = "Mehr Patientenkomfort durch kürzere Wartezeiten"
[[blocks.slider]]
heading = "EINFACH UND DIGITAL"
slider_image = "/uploads/2018/06/11/slider2.jpg"
subheading = "Unkomplizierte Online-Bestellung ohne Rückfragen"
[[blocks.slider]]
heading = "KUNDENORIENTIERT"
slider_image = "/uploads/2018/06/11/slider3.jpg"
subheading = "Schnelle Prozesse für medizinische Einrichtungen"
[[blocks.slider]]
heading = "EINE PLATTFORM"
slider_image = "/uploads/2018/06/11/slider4.jpg"
subheading = "Für Patienten, medizinsche Einrichtungen und Transportunternehmen"
[[blocks]]
template = "timeline"
[[blocks.timeline_elements]]
Imageicon = "/uploads/2018/06/12/entry1.png"
imageicon_color = "#09C2FA"
text = "## **Die Idee wird geboren**\n\nBeim gemütlichen Angrillen im März fördern ein paar Bier auch mal die Kreativität. Aus zwei Einzelpersonen wurde eine Geschäftsidee, aus Christian und Alexander wurde CIRIS. Es folgten mehrere Monate theoretischer Arbeit unter anderem an Konzept, Businessplan und Geschäftsmodell."
title_date = "März 2017"
[[blocks.timeline_elements]]
Imageicon = "/uploads/2018/06/12/icons8-ok-32.png"
imageicon_color = "#0390CF"
text = "## **Erster Realitätscheck**\n\nUm einen ersten Realitätscheck durchzuführen und etwas Feedback von externen Parteien zu bekommen, meldete sich CIRIS bei einigen Startup-Wettbewerben an. Nach vielen Bewertungen und Verbesserungsvorschlägen konnten erste Top10-Platzierungen (u.a. bei Science4Life) erreicht werden."
title_date = "September 2017"
[[blocks.timeline_elements]]
Imageicon = "/uploads/2018/06/12/icons8-training-50.png"
imageicon_color = "#09C2FA"
text = "## **Das Pitchen geht los**\n\nNach der Einladung vom CyberForum hatte CIRIS die Möglichkeit, vor 20 Investoren aus verschiedensten Branchen zu pitchen. Jeweils 6 Minuten entschieden, ob es zum Austausch der Kontaktdaten kam. CIRIS konnte direkt 4 Investoren überzeugen und es wurden Follow-Up Meetings angesetzt."
title_date = "Oktober 2017"
[[blocks.timeline_elements]]
Imageicon = "/uploads/2018/06/12/icons8-people-50.png"
imageicon_color = "#09C2FA"
text = "## **Das Team wird größer**\n\nWie es bereits im Forbes Magazin stand: \"To run an efficient team, you only need three people: a Hipster, a Hacker, and a Hustler.\" Um alle dieser Kompetenzen im Team zu verkörpern, holte sich CIRIS noch einen professionellen Entwickler ins Team."
title_date = "November 2017"
[[blocks.timeline_elements]]
Imageicon = "/uploads/2018/06/12/icons8-rocket-50.png"
imageicon_color = "#0057A0"
text = "## **Jahresabschluss**\n\nZum Jahresabschluss nimmt CIRIS nochmal an Fahrt auf. Der erste Prototyp ist fertig, die Android-App ist im Google Play Store erhältlich, Beteiligungsverträge wurden erstellt, das Marketing-Konzept von Agenturen geprüft, Angebote eingeholt und die Homepage ist online."
title_date = "Dezember 2017"
[[blocks.timeline_elements]]
Imageicon = "/uploads/2018/06/12/icons8-collaboration-50.png"
imageicon_color = "#0057A0"
text = "## **CIRIS erste Kunden**\n\nCIRIS wurde von mehreren Patiententransport-Unternehmen eingeladen und hatte die Gelegenheit, die Plattform vorzustellen. Es kommt sehr interessantes Feedback und zusätzliche Ideen seitens der Kunden. Die Letter of Intent wurden unterschrieben, da der Mehrwert von CIRIS den Kunden ersichtlich ist!"
title_date = "Februar 2018"
[[blocks.timeline_elements]]
Imageicon = "/uploads/2018/06/12/icons8-money-50.png"
imageicon_color = "#0057A0"
text = "## **Investoren und Politiker**\n\nDer Aufbau des Netzwerks wird verstärkt. Es findet vermehrt Austausch mit renommierten Business Angels und Venture Capitals (Digital Health Ventures, High-Tech Gründerfonds) statt. CIRIS bekommt zudem eine Rückmeldung vom Bundeskanzleramt auf eine Anfrage zum Austausch."
title_date = "April 2018"
[[blocks.timeline_elements]]
Imageicon = "/uploads/2018/06/12/entry8.png"
imageicon_color = "#09C2FA"
text = "## **Netzwerk mit Healthcare Companies**\n\nUm die Skalierung nach dem Proof of Concept zu forcieren, wurde Kontakt zu Healthcare Companies mit zusätzlichem Kliniknetzwerk aufgenommen. Von diesem Netzwerk möchte CIRIS in der Skalierungsphase profitieren. Beide Parteien waren interessiert und luden CIRIS zu einem Gespräch ein."
title_date = "Mai 2018"
[[blocks.timeline_elements]]
Imageicon = "/uploads/2018/06/12/entry8.png"
imageicon_color = "#09C2FA"
text = "## **CIRIS auf dem Pirate Summit in Köln**\n\nDie Startup-Unit der Stadt Köln hat die 10 besten und vielversprechendsten Digital Health Startups gesucht! CIRIS war eines davon. So waren wir mit Demo und jeder Menge Aufregung vor Ort und konnten unsere Lösung verschiedenen Experten, Investoren und Interessierten zeigen. Wenn ihr mehr über den Pirate-Summit erfahren wollt, schaut auf: [https://piratesummit.com/digitalhealthvillage/](https://piratesummit.com/digitalhealthvillage/ \"https://piratesummit.com/digitalhealthvillage/\")\n\n![](/uploads/2019/10/08/Pirate-summit.jpg)"
title_date = "Juli 2019"
[[blocks.timeline_elements]]
Imageicon = "/uploads/2018/06/12/icons8-ok-32.png"
imageicon_color = "#09C2FA"
text = "## **FINALE: CIRIS nominiert für den Health-i Award**\n\nDie Techniker Krankenkasse und das Handelsblatt haben die Health-i Initiative ins Leben gerufen. Mit ihr wollen Sie die besten Talente in den Bereichen Gesundheit und Gesundheitswirtschaft, die vielversprechendsten Start-ups und die innovativsten Denker Deutschlands entdecken und fördern. Sie wollen die Chancen der innovativen Projekte und Produkte im Markt in den Mittelpunkt stellen, gemeinsam mit ihnen an Konzepten feilen und den Austausch mit den richtigen Partnern ermöglichen. CIRIS ist unter den ersten Drei, in der Kategorie Startup. Im November werden wir daher in Berlin vor dem Heatlh-i Board pitchen und unsere Lösung für besseren Patiententransport präsentieren. Alles zum Nachlesen über den Health-i Award findet ihr auf: [http://health-i.de/de/](http://health-i.de/de/ \"http://health-i.de/de/\")\n\n![](/uploads/2019/10/08/health-i.jpg)"
title_date = "September 2019"

+++
